package lab6;
import java.util.Arrays;

	
	public class Implementation implements CharSequence {
		
		private char[] chars;
		
		public Implementation(char[] chars) {
			this.chars=chars;
		}
		
		public char[] returns() {
			return this.chars;
		}
		
		@Override
		public char charAt(int index) {
			if(index<0 && index>chars.length)
				System.exit(0);
			return chars[index];
		}
		 
		@Override
		public int length(){
			return chars.length;
		}
	   
		@Override
		public CharSequence subSequence(int start, int end) {
			if(start<0 && start>chars.length)
				System.exit(0);
			if(end<0 && end>chars.length)
				System.exit(0);
			/*
			char[] s=Arrays.copyOfRange(chars, start, end);
			this.chars=s;
			return null;
			*/
			
			String s=new String();
			for(int i=start;i<end;i++)
				s=s+chars[i];
			return s;
		}
	   
	}



package lab6;

	import static org.junit.Assert.assertEquals;
   import static org.junit.jupiter.api.Assertions.*;

	import java.util.Arrays;

	import org.junit.jupiter.api.Test;

	class ImplementationTest{
		
		char[] array= {'a','b','c','d','e','f','g'};
		Implementation imp=new Implementation(array);
		
		@Test
		void testCharAt() {
			assertEquals(array[2],imp.charAt(2));
		}

		@Test
		void testLength() {
			assertEquals(7,imp.length());
		}

		@Test
		void testSubSequence() {
			assertEquals("de",imp.subSequence(3, 5));
			
		}


	}


