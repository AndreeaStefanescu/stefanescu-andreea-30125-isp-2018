package lab6;
import java.awt.*;
public class Circle implements Shape{
	
	    private int radius;
	    
	    private Color color;
	    private int x; //coords
	    private int y;
	    private String id;
	    private boolean filled;

	    public Circle(Color color,int x,int y,String id,boolean filled, int radius) {
	    	this.color = color;
	        this.x=x;
	        this.y=y;
	        this.id=id;
	        this.filled=filled;
	        this.radius = radius;
	    }

	    public int getRadius() {
	        return radius;
	    }

	    @Override
	    public void draw(Graphics g) {
	        System.out.println("Drawing a circle "+this.radius+" "+this.color);
	        g.setColor(this.color);
	        g.drawOval(this.x,this.y,radius,radius);
	        if(this.filled)
	        g.fillOval(this.x,this.y, radius, radius);
	    }
	    
	    public String getId() {
	    	return this.id;
	    }
	    
	}



package lab6;
import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class DrawingBoard  extends JFrame{

	    Shape[] shapes = new Shape[10];
	    //ArrayList<Shape> shapes = new ArrayList<>();

	    public DrawingBoard() {
	        super();
	        this.setTitle("Drawing board example");
	        this.setSize(300,500);
	        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
	        this.setVisible(true);
	    }

	    public void addShape(Shape s1){
	        for(int i=0;i<shapes.length;i++){
	            if(shapes[i]==null){
	                shapes[i] = s1;
	                break;
	            }
	        }
//	        shapes.add(s1);
	        this.repaint();
	    }

	    public void paint(Graphics g){
	        for(int i=0;i<shapes.length;i++){
	            if(shapes[i]!=null)
	                shapes[i].draw(g);
	        }
//	        for(Shape s:shapes)
//	            s.draw(g);
	    }
	    
	    public void delete(String id){
	    	for(int i=0;i<shapes.length;i++)
	    		if(shapes[i]!=null && shapes[i].getId().equals(id))
	    			shapes[i]=null;
	    		this.repaint();
	    }
	}



package lab6
import java.awt.*;
public class Rectangle implements Shape{

	    private int length;
	    
	    private Color color;
	    private int x; //coords
	    private int y;
	    private String id;
	    private boolean filled;

	    public Rectangle(Color color,int x,int y,String id,boolean filled ,int length) {
	    	this.color = color;
	        this.x=x;
	        this.y=y;
	        this.id=id;
	        this.filled=filled;
	        this.length = length;
	    }

	    @Override
	    public void draw(Graphics g) {
	        System.out.println("Drawing a rectangle "+length+" "+this.color);
	        g.setColor(this.color);
	        g.drawRect(this.x,this.y, length, length/2);
	        if(this.filled)
	            g.fillRect(this.x,this.y, length, length/2);
	    }
	    
	    public String getId() {
	    	return this.id;
	    }
	}



package lab6
import java.awt.*;

	public interface Shape {

	    public void draw(Graphics g);
	    public String getId();
	}



package lab6;
import java.awt.*;
public class Main {

	/**
	 * @author mihai.hulea
	 */
	    public static void main(String[] args) {
	        DrawingBoard b1 = new DrawingBoard();
	        Shape s1 = new Circle(Color.RED,47,25,"aa1",true, 85);
	        b1.addShape(s1);
	        Shape s2 = new Circle(Color.GREEN,7,15,"34b",false, 56);
	        b1.addShape(s2);
	        Shape s3= new Rectangle(Color.BLUE,71,63,"b5",true,41);
	        b1.addShape(s3);
	        //b1.delete("b5");
	    }
	}
