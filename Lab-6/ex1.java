package lab6;

import java.awt.*;

public class Circle extends Shape{

    private int radius;
    private int dx;
    private int dy;
   

    public Circle(int dx, int dy, Color color, int radius) {
        super(color);
        this.radius = radius;
        this.dx = dx;
        this.dy = dy;
    }

    public int getRadius() {
        return radius;
    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a circle "+this.radius+" "+getColor().toString());
        g.setColor(getColor());
        g.drawOval(dx,dy,radius,radius);
    }
}

package lab6;

import java.awt.*;

public class Rectangle extends Shape{

    private int length;
    private int dx;
    private int dy;
    private int height;


    public Rectangle(int dx, int dy, Color color, int length, int height) {
        super(color);
        this.length = length;
        this.dx = dx;
        this.dy = dy;
        this.height=height;
    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a rectangel "+length+" "+getColor().toString());
        g.setColor(getColor());
        g.drawRect(dx, dy, length,height);
    }
}

package lab6;

import java.awt.*;

public abstract class Shape {
	
    private Color color;
    String id;

    public Shape(Color color) {
        this.color = color;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public abstract void draw(Graphics g);
}

package lab6;

import java.awt.*;

/**
 * @author mihai.hulea
 */
public class Main {
    public static void main(String[] args) {
        DrawingBoard b1 = new DrawingBoard();
        Shape s1 = new Circle(70,70,Color.RED, 90);
        b1.addShape(s1);
        Shape s2 = new Circle(140,140,Color.GREEN, 100);
        b1.addShape(s2);
        Shape s3 = new Rectangle(100,100,Color.BLUE, 110,20);
        b1.addShape(s3);
    }
}

package lab6;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class DrawingBoard  extends JFrame {

    Shape[] shapes = new Shape[10];
    public int i;
    //ArrayList<Shape> shapes = new ArrayList<>();

    public DrawingBoard() {
        super();
        this.setTitle("Drawing board example");
        this.setSize(300,500);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    public void addShape(Shape s1){
        for(int i=0;i<shapes.length;i++){
            if(shapes[i]==null){
                shapes[i] = s1;
                break;
            }
        }
//        shapes.add(s1);
        this.repaint();
    }

    public void paint(Graphics g){
        for(int i=0;i<shapes.length;i++){
            if(shapes[i]!=null)
                shapes[i].draw(g);
        }
    }
    public void delete(String id){
    	for (i=0;i<shapes.length;i++){
    		if (shapes[i].id.equals(id))
    			shapes[i]=null;
    	this.repaint();
    	}
//        for(Shape s:shapes)
//            s.draw(g);
    }
}