package g30125.stefanescu.andreea.l2.e5;

public class ex5 {
	static void bubbleSort(int[] arr) {  
        int n = arr.length;  
        int aux = 0;  
         for(int i=0; i < n; i++){  
                 for(int j=1; j < (n-i); j++){  
                          if(arr[j-1] > arr[j]){   
                                 aux = arr[j-1];  
                                 arr[j-1] = arr[j];  
                                 arr[j] = aux;  
                         }    
                 }
         }
	}
      public static void main(String[] args) {  
      int arr[] ={50,34,342,123,4,32,5,7,2,19};     
      System.out.println("Sirul inainte de ordonare");  
      for(int i=0; i < arr.length; i++){  
              System.out.print(arr[i] + " ");  
         }  
        System.out.println();  
       bubbleSort(arr);   
       System.out.println("Sirul dupa ordonare");  
       for(int i=0; i < arr.length; i++){  
              System.out.print(arr[i] + " ");  
            }              
}
}