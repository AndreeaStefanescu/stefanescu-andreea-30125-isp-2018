package g30125.stefanescu.andreea.l2.e4;

import java.util.Scanner;

public class ex4 {
	public static void main(String[] args){
		Scanner in= new Scanner(System.in);
		System.out.println("Numarul de elemente din vector= ");
		int i;
		int max=0;
		int a[]=new int[20];
		int n=in.nextInt();
		for (i=1;i<=n;i++){
			System.out.println("Introduceti elementul " + i +":");
			a[i]=in.nextInt();
		}
		for (i=1;i<=n;i++){
			if (a[i]>max)
				max=a[i];
		}
		System.out.println("Elementul maxim din vector este: " + max);
		in.close();
	}
}