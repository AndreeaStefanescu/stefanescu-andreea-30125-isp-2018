package g30125.stefanescu.andreea.l2.e6;
import java.util.Scanner;
public class ex6 {
    static void nerecursiv(int n){
    	int s=1;
    	for(int i=1;i<=n;i++)
    		s=i*s;
    	System.out.println("N!= "+s);
    }
    static int recursiv(int n){
    	if(n==0)
    		return 0;
    	else
    		return recursiv(n * recursiv(n-1));
    }
    public static void main(String[] args){
    	Scanner in = new Scanner(System.in);
    	System.out.println("Valoarea lui N:");
    	int n=in.nextInt();
    	System.out.println("N! calculat nerecursiv: ");
    	nerecursiv(n);
    	int k = recursiv(n);
    	System.out.println("N! calculat recursiv: "+k);
    	in.close();
    }
}

