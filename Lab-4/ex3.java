package ex3;

public class circle {
	private double radius;
	private String color;
	
	public circle()
	{
		radius = 1.0;
		color = "red";
	}
	public circle(double raza)
	{
		radius = raza;
		color = "red";
	}
	
	public double getRadius()
	{
		return radius;
	}
	
	public double getArea()
	{
		  return 3.14*Math.pow(radius,2);
	}
	public static void main(String[] args)
	{
		circle cerc = new circle(5.4);
		System.out.println(cerc.getRadius());
		System.out.println(cerc.getArea());
	}
}
