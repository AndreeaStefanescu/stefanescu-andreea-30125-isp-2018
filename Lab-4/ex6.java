package Ex6;

public class Author {
	String name;
	String email;
	char gender;
	
	
	Author(String name,String email,char gender){
		this.name=name;
		this.email=email;
		this.gender=gender;
	}
	
	String getName() {
		return name;
	}
	
	String getEmail() {
		return email;
	}
	
	void setEmail(String newemail) {
		this.email=newemail;
	}
	
	char getGender() {
		return gender;
	}
}

package Ex6;


public class Book {
	String nume;
	private static Author[] authors;
	double pret;
	int cantitate;
	
	public Book(String nume,Author[] authors,double pret,int cantitate) {
		this.nume=nume;
		authors=new Author[2];
		authors[0]=new Author("Eminescu","email",'m');
		authors[1]=new Author("Creanga","gmail",'m');
		this.pret=pret;
		this.cantitate=cantitate;
	}
	
	public Book(String nume,Author[] authors,double pret) {
		this.nume=nume;
		authors=new Author[2];
		authors[0]=new Author("Eminescu","eminescu@email.com",'m');
		authors[1]=new Author("Creanga","creanga@gmail.com",'m');
		this.pret=pret;
	}
	
	String getName() {
		for(int i=0;i<2;i++) {
		System.out.println( authors[i].name);
		}
		return authors[1].name;
	}
	
	Author getauthors() {
		for(int i=0;i<2;i++) {
		System.out.println(authors[i]);
	   }
		return authors[1];
	}
	
	double getpret() {
		System.out.println(pret);
		return pret;
	}
	
	void setpret(double pretnou) {
		this.pret=pretnou;
	}
	
	int getcantitate() {
		System.out.println(cantitate);
		return cantitate;
	}
	
	void setcantitate(int cantitatenoua) {
		this.cantitate=cantitatenoua;
	}
	
	void printAuthors() {
		for (int i=0;i<2;i++) {
			System.out.println(authors[i]);
		}
	}

}