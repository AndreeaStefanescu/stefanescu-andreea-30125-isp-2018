package Carte;

public class Book {
	String nume;
	String autor;
	double pret;
	int cantitate;
	
	public Book(String nume,String autor,double pret,int cantitate) {
		this.nume=nume;
		this.autor=autor;
		this.pret=pret;
		this.cantitate=cantitate;
	}
	
	public Book(String nume,String autor,double pret) {
		this.nume=nume;
		this.autor=autor;
		this.pret=pret;
	}
	
	double getpret() {
		System.out.println("pret");
		return pret;
	}
	
	void setpret(double pretnou) {
		this.pret=pretnou;
	}
	
	String getautor() {
		System.out.println("autor");
		return autor;
	}
	
	void setautor(String autornou) {
		this.autor=autornou;
	}
	
	int getcantitate() {
		System.out.println("cantitate");
		return cantitate;
	}
	
	void setcantitate(int cantitatenoua) {
		this.cantitate=cantitatenoua;
	}

	public static void main(String[] args) {
		Book b=new Book("Ion","Liviu",12000);
		b.setautor("Liviu Rebreanu");
		b.getautor();
		b.getpret();
		b.setpret(13500);
		b.getpret();
		Book B=new Book("Baltagul","Sadoveanu",10000,30);
		B.getautor();
		
	}
}