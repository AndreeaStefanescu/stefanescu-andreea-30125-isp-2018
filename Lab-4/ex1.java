package stefanescu.andreea;

	public class Box {
	    private int id;

	    public Box(Conveyor target, int pos, int id){
	        this.id = id;
	        target.addPackage(this,pos);
	    }

	    public int getId() {
	        return id;
	    }

	    public String toString(){
	        return ""+id;
	    }
	}

package stefanescu.andreea;

public class Conveyor {

    private Box[] packages = new Box[30];

    void addPackage(Box p, int pos){
        packages[pos] = p;
    }

    public void moveLeft(){
        for(int i=1;i<packages.length-1;i++){
            packages[i] = packages[i+1];
        }
        packages[packages.length-1] = null;

    }

    public void moveRight() {
        for (int i = packages.length - 1; i > 0; i--) {
            packages[i] = packages[i - 1];
        }
        packages[0] = null;
    }

    public Box getBox(int pos){
        return packages[pos];
    }

    public void display(){
        int k = 0;
        for (int i = 0; i < packages.length; i++) {
            String c = (packages[i]!=null)?packages[i].toString():"_";
            System.out.print(c);
        }
        System.out.println();
    }
}

package stefanescu.andreea;

public class Main {

    public static void main(String[] args) {
        Conveyor cb1 = new Conveyor();
        Box p1 = new Box(cb1, 0, 7);
        Box p2 = new Box(cb1, 10, 3);


        cb1.display();
        cb1.moveRight();
        cb1.display();
        cb1.moveRight();
        cb1.display();
        cb1.moveRight();
        cb1.display();
        cb1.moveRight();
        cb1.display();
        cb1.moveLeft();
        cb1.display();
        cb1.moveLeft();
        cb1.display();
        cb1.moveLeft();
        cb1.display();
        cb1.moveLeft();
        cb1.display();
        cb1.moveLeft();
        cb1.display();
        cb1.moveLeft();
        cb1.display();
        cb1.moveLeft();
        cb1.display();
    }
}

package stefanescu.andreea;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author mihai.hulea
 */
public class TestConveyor {

    @Test
    public void shouldAddBox(){
        Conveyor c = new Conveyor();
        Box b = new Box(c,0,1);
        assertEquals(b.getId(), c.getBox(0).getId());
    }

    @Test
    public void shouldMoveBoxToRight(){
        Conveyor c = new Conveyor();
        Box b = new Box(c,1,1);
        c.moveRight();
        assertEquals(b.getId(), c.getBox(2).getId());
    }

    @Test
    public void shouldMoveBoxToLeft(){
        Conveyor c = new Conveyor();
        Box b = new Box(c,0,1);
        c.moveLeft();
        assertEquals(b.getId(), c.getBox(0).getId());

    }

}

