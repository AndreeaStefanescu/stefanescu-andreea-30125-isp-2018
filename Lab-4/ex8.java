package ex8;

public class Shape {
	String color="red";
	boolean filled=true;
	
	Shape(){
		
	}
	
	Shape(String color,boolean filled){
		this.color=color;
		this.filled=filled;
	}
	
	String getColor() {
		return color;
	}
	
	void setColor(String newcolor) {
		this.color=newcolor;
	}
	
	boolean isFilled() {
		return filled;
	}
	
	void setFilled(boolean newfilled) {
		this.filled=newfilled;
	}
	
	public static void main(String[] args) {
		Square s=new Square();
		s.setWidth(123);
	}

}

package ex8;

public class Circle extends Shape{
	private double radius=1.0;
	
	Circle(){
		super();
	}
	
	Circle(double radius){
		this.radius=radius;
	}
	
	Circle(double radius,String color,boolean filled){
		super();
		this.radius=radius;
	}
	
	double getRadius() {
		return radius;
	}
	
	void setRadius(double newradius) {
		this.radius=newradius;
	}
	
	double getArea() {
		return 3.14*radius*radius;
	}
	
	double getPerimeter() {
		return 3.14*2*radius;
	}

}
package ex8;

public class Rectangle extends Shape{
	double width=1.0;
	double length=1.0;
	
	Rectangle(){
		super();
	}
	
	Rectangle(double width,double length){
		this.width=width;
		this.length=length;
	}
	
	Rectangle(double width,double length,String color,boolean filled){
		super();
		this.width=width;
		this.length=length;
	}
	
	double getWidth() {
		return width;
	}
	
	void setWidth(double newwidth) {
		this.width=newwidth;
	}
	
	double getLength() {
		return length;
	}
	
	void setLength(double newlength) {
		this.length=newlength;
	}
	
	double getArea() {
		return width*length;
	}
	
	double getPerimetter() {
		return 2*(length+width);
	}
	
	
}

package ex8;

public class Square extends Rectangle{
    double side; 
	
	Square(){
		super();
	}
	
	Square(double side){
		this.side=side;
	}
	
	Square(double side,String color,boolean filled){
		super();
		this.side=side;
	}
	
	double getSide() {
		return side;
	}
	
	void setSide(double newside) {
		this.side=newside;
	}
	
   void setWidth(double newwidth) {
	 super.setWidth(newwidth);
   }
   
   void setLength(double newlength) {
	   super.setLength(newlength);
   }
   
 }
