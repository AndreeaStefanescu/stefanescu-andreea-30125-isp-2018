package ex4;

public class Author(
	String name;
	String email;
	char gender;

	Author(String name, String email, char gender){
		this.name=name;	
		this.email=email;
		this.gender=gender;
	}

	String getName(){
		return name;
}
	String getEmail(){
		return email;
	}
	void setEmail(String newemail){
		this.email=newemail;
}
	char getGender(){
		return gender;
}
	String toString(){
		return name;
		return email;
		return gender;
}

public static void main(String[] args){
	Author a=new Author("Mihai Eminescu","eminescu.mihai@gmail.com","m");
	a.getEmail();
	a.setEmail("mihai.e@gmail.com");
}
}
	