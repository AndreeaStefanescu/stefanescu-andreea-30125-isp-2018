package ex7;

public class Circle {
	double radius=1.0;
	String color="red";
	
	Circle(){
		
	}
	
	Circle(double radius){
		this.radius=radius;
	}
	
	public double getRadius() {
		return radius;
	}
	
	public double getArea() {
		return 3.14*radius*radius;
	}

}
public class Cylinder extends Circle {
	double height=1.0;

	Cylinder(){
		
	}
	
	Cylinder(double radius){
		super();
	}
	
	Cylinder(double radius,double height){
		super();
		this.height=height;
	}
	
	double getHeight() {
		return height;
	}
	
	double getVolume() {
		return 3.14*radius*radius*height;
	}
}