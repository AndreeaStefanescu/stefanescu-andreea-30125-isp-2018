package lab5;

public interface Image {
	void display();

}


package lab5;

public class ProxyImage implements Image{
	
	private RealImage realImage;
	private RotatedImage rotatedImage;
	private String fileName;
	  private int choose;
	 
	   public ProxyImage(String fileName,int choose){
	      this.choose=choose;
	      this.fileName=fileName;
	   }
	   

	@Override
	   public void display() {
		   
	         realImage = new RealImage(fileName);
	    	 rotatedImage=new RotatedImage(fileName);
	      
	    	
	   
	     if(choose==1) {
	    	 if(realImage == null){
	             realImage = new RealImage(fileName);
	          }
	          realImage.display();
	    	
	     }
	     else if(choose==2) {
	    	 if(rotatedImage == null) {
	        	  rotatedImage = new RotatedImage(fileName);
	          }
	    	 rotatedImage.display();
	     }
	    	 
	    	  
	   }

}



package lab5;

public class RealImage implements Image{
	private String fileName;
	 
	   public RealImage(String fileName){
	      this.fileName = fileName;
	      loadFromDisk(fileName);
	   }
	 
	   @Override
	   public void display() {
	      System.out.println("Displaying " + fileName);
	   }
	 
	   private void loadFromDisk(String fileName){
	      System.out.println("Loading " + fileName);
	   }
	}


package lab5;

public class RotatedImage implements Image{
	private String fileName;
	
	public RotatedImage(String fileName) {
		this.fileName=fileName;
		loadFromDisk(fileName);
	}
	public void display() {
		System.out.println("Display rotated "+fileName);
	}
	private void loadFromDisk(String fileName){
	      System.out.println("Loading " + fileName);
	   }
}


package lab5;

public class Test {
	public static void main(String[] args) {
		RotatedImage ri=new RotatedImage("74142_l.jpeg");
		ri.display();
		
		RealImage re=new RealImage("3.png");
		re.display();
		
		System.out.println("------------");
		ProxyImage pi=new ProxyImage("74142_l.jpeg",2);
		pi.display();
	}
	

}