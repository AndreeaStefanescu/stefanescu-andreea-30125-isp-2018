package lab5;

public abstract class Shape {
	protected String color;
	protected boolean filled;
	public Shape(){
		this.color="red";
		this.filled=true;
	}
	public Shape(String color, boolean filled){
		this.color=color;
		this.filled=filled;
	}
	public String getColor(){
		return color;
	}
	public void setColor(String color){
		this.color=color;
	}
	boolean isFilled(){
		return filled;
	}
	public void setFilled(boolean filled){
		this.filled=filled;
	}
	public abstract double getArea();
	public abstract double getPerimeter();
@Override
public String toString(){
	return "Shape{" + "color='" + color + '\'' + ", filled=" + filled + '}';
}
}

package lab5;

public class Circle extends Shape {
	protected double radius;
	public Circle(){
	}
	public Circle(double radius){
		this.radius=radius;
	}
	public Circle(double radius, String color, boolean filled){
		super(color,filled);
		this.radius=radius;
	}
	public double getRadius(){
		return radius;
	}
	public void setRadius(double radius){
		this.radius=radius;
	}
	public double getArea(){
		return 3.14*radius*radius;
	}
	public double getPerimeter(){
		return 2*3.14*radius;
	}
	public String toString(){
		return "Circle{" + "color='" + color + '\'' + ", filled=" + filled + ", radius=" + radius + '}';
	   }
}

package lab5;

public class Rectangle extends Shape {
		protected double width;
		protected double length;
		public Rectangle(){
		}
		public Rectangle(double width,double length){
			this.width=width;
			this.length=length;
		}
		public Rectangle(double width, double length, String color, boolean filled){
			super(color,filled);
			this.width=width;
			this.length=length;
		}
		public double getWidth(){
			return width;
		}
		public void setWidth(double width){
			this.width=width;
		}
		public double getLength(){
			return length;
		}
		public void setLength(double length){
			this.length=length;
		}
		public double getArea(){
			return length*width;
		}
		public double getPerimeter(){
			return 2*(width+length);
		}
		public String toString(){
			return "Rectangle{"+ "color='" + color + '\'' + ", filled=" + filled + "width='" + width + '\'' + ", length=" + length + '}';
		   }
}

package lab5;

public class Square extends Rectangle {
	double side;
	public Square(){
	}
	public Square(double side){
		this.side=side;
	}
	public Square(double side, String color, boolean filled){
		this.color=color;
		this.filled=filled;
		this.side=side;
	}	
}



package lab5;

public class TestProgram {
	public static void main(String[] args){
		System.out.println("------Circle------");
		Circle c=new Circle(1.5,"blue",true);
		System.out.println(c.toString());
		System.out.println("Aria="+c.getArea());
		System.out.println("Perimetrul="+c.getPerimeter());
		
		
		
		 System.out.println("-------Rectangle----------");
	  	  Rectangle r1=new Rectangle(1.5,1.5,"red",false);
		  System.out.println(r1.toString());
		  System.out.println("Aria="+r1.getArea());
		  System.out.println("Perimetrul="+r1.getPerimeter());
		  
		  
		  
		  
		  System.out.println("-------Square----------");
		  Square p1=new Square(2.0,"yellow",true);
	  	  System.out.println(p1.toString());
		System.out.println("Perimetrul="+p1.getPerimeter());
	}

}

