package lab7;

	import java.util.ArrayList;
	import java.util.Collections;
	import java.util.Comparator;
	import java.util.List;

	public class Bank {

		 static ArrayList accounts = new ArrayList();

		    public static void getAllAcounts()
		    {
		        Collections.sort(accounts, new Comparator<BankAccount>() {
		            @Override
		            public int compare(BankAccount arg0, BankAccount arg1) {
		                String s0=arg0.getOwner();
		                String s1=arg1.getOwner();
		                return s0.compareToIgnoreCase(s1);
		            }
		        });
		        for(Object o:accounts){
		            System.out.println(o.toString());
		        }
		    }

		    public static void addAcount(String owner,double balance)
		    {
		        BankAccount p1 = new BankAccount(owner,balance);
		        accounts.add(p1);
		    }

		    public static void printAccounts(double minBalance, double maxBalance)
		    {
		        System.out.println("\nAccounts between "+minBalance+" and "+maxBalance+" are:");
		        for(int i=0;i<accounts.size();i++)
		        {
		            BankAccount a = (BankAccount) accounts.get(i);
		            if(a.getBalance()>minBalance && a.getBalance()<maxBalance)
		            {
		                System.out.println(a.toString());
		            }
		        }
		    }

		    public static void printAccounts()
		    {
		        Collections.sort(accounts, new Comparator<BankAccount>() {
		            @Override
		            public int compare(BankAccount arg0, BankAccount arg1) {
		                if(arg0.getBalance()>arg1.getBalance()) return 1;
		                if(arg0.getBalance()==arg1.getBalance()) return 0;
		                return -1;
		            }
		        });

		        for(Object o:accounts){
		            System.out.println(o.toString());
		        }
		    }

		    public static BankAccount getAccount(String owner)
		    {
		        for(int i=0;i<accounts.size();i++)
		        {
		            BankAccount a = (BankAccount) accounts.get(i);
		            if (a.getOwner().equals(owner))
		            {
		                System.out.println("\nCont gasit!");
		                return a;
		            }
		        }
		        System.out.println("\nNici un cont cu numele "+owner+" nu a fost gasit");
		        return null;
		    }
		    public static void main(String[] args)
		    {
		        addAcount("Andrei",423);
		        addAcount("matei",69);
		        addAcount("dan",70);

		        printAccounts();
		        printAccounts(10,100);
		        System.out.println("\n");
		        getAllAcounts();
		        System.out.println(getAccount("Andrei"));
		    }


	}



package lab7;

public class BankAccount {
	
		String owner;
		private double balance;
		
		public BankAccount(String owner, double balance) {
			this.owner = owner;
			this.balance = balance;
		}
		
		public String getOwner() {
			return owner;
		}
		public void setOwner(String owner) {
			this.owner = owner;
		}
		public double getBalance() {
			return balance;
		}
		public void setBalance(double balance) {
			this.balance = balance;
		}
	public void withdraw(double amount){
		setBalance(getBalance()-amount);	
		}
		public void deposit (double amount){
			setBalance(getBalance()+amount);	
		}
		@Override
		public boolean equals(Object o){
			if(o instanceof BankAccount){
				BankAccount b=(BankAccount)o;
				return owner ==b.owner && balance==b.balance;
			}
			return false;
		}
		@Override
		public int hashCode(){
			return (int) balance+owner.hashCode();
		}	
		public String toString() {
	        return "Nume: "+owner+", venit: "+balance;
	    }

	}

