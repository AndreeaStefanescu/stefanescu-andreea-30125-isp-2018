package lab7;


	import java.util.Comparator;
	import java.util.Iterator;
	import java.util.TreeSet;



	public class Bank {
		 private static TreeSet<BankAccount> accounts = new TreeSet<BankAccount>(new MyValComp());
		    private static TreeSet<BankAccount> accounts2 = new TreeSet<BankAccount>(new MyOwnerComp());

		    static class MyOwnerComp implements Comparator<BankAccount>
		    {
		        @Override
		        public int compare(BankAccount e1, BankAccount e2) {
		           return e1.getOwner().compareTo(e2.getOwner());

		        }
		    }

		    static class MyValComp implements Comparator<BankAccount>
		    {
		        @Override
		        public int compare(BankAccount e1, BankAccount e2) {
		            if(e1.getBalance() > e2.getBalance()){
		                return 1;
		            } else {
		                return -1;
		            }
		        }
		    }

		    public static void getAllAcounts() //ordine alfabetica
		    {
		        for(Object o:accounts2){
		            System.out.println(o.toString());
		        }
		    }

		    public static void addAcount(String owner,double balance)
		    {
		        accounts.add(new BankAccount(owner,balance)); //sortate direct dupa campul balance
		        accounts2.add(new BankAccount(owner,balance)); //sortate direct in ordine alfabetica
		    }

		    public static void printAccounts(double minBalance, double maxBalance)
		    {
		        System.out.println("\nAccounts between "+minBalance+" and "+maxBalance+" are:");
		        Iterator<BankAccount> iterator = accounts.iterator();
		        while(iterator.hasNext())
		        {
		            BankAccount node = iterator.next();
		            if(node.getBalance() > minBalance && node.getBalance()<maxBalance)
		                System.out.println(node.toString());
		        }
		    }

		    public static void printAccounts()
		    {

		        for(Object o:accounts){
		            System.out.println(o.toString());
		        }
		    }

		    public static BankAccount getAccount(String owner)
		    {
		        Iterator<BankAccount> iterator = accounts.iterator();
		        while(iterator.hasNext())
		        {
		            BankAccount node = iterator.next();
		            if (node.getOwner().equals(owner)) {
		                System.out.println("\nCont gasit!");
		                return node;
		            }
		        }
		        System.out.println("\nNici un cont cu numele "+owner+" nu a fost gasit");
		        return null;
		    }

		    public static void main(String[] args)
		    {
		        addAcount("marcel",45123);
		        addAcount("dan",69);
		        addAcount("aurel",70);
		        addAcount("cristi",45);

		        System.out.println("TestPrintAccounts - ordine crescatoare dupa Balance");
		        printAccounts();
		        printAccounts(10,100);
		        System.out.println("\nTest GetAllAccounts - ordine alfabetica");
		        getAllAcounts();
		        System.out.println(getAccount("marcel"));
		    }


	}



package lab7;

public class BankAccount {
	
		String owner;
		private double balance;
		
		public BankAccount(String owner, double balance) {
			this.owner = owner;
			this.balance = balance;
		}
		
		public String getOwner() {
			return owner;
		}
		public void setOwner(String owner) {
			this.owner = owner;
		}
		public double getBalance() {
			return balance;
		}
		public void setBalance(double balance) {
			this.balance = balance;
		}
	public void withdraw(double amount){
		setBalance(getBalance()-amount);	
		}
		public void deposit (double amount){
			setBalance(getBalance()+amount);	
		}
		@Override
		public boolean equals(Object o){
			if(o instanceof BankAccount){
				BankAccount b=(BankAccount)o;
				return owner ==b.owner && balance==b.balance;
			}
			return false;
		}
		@Override
		public int hashCode(){
			return (int) balance+owner.hashCode();
		}	
		public String toString() {
	        return "Nume: "+owner+", venit: "+balance;
	    }


	}
