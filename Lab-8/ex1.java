package lab8;

public class Coffee {
	
	      private int temp;
	      private int conc;
	      private int sugar;
	 
	      Coffee(int t,int c,int su){temp = t;conc = c;sugar = su;}
	      int getTemp(){return temp;}
	      int getConc(){return conc;}
	      int getSugar() {return sugar;}
	      public String toString(){return "[cofee temperature="+temp+":concentration="+conc+":sugar="+sugar+"]";}
		}


package lab8;

public class CoffeeDrinker {

    void drinkCoffee(Coffee c) throws TemperatureException, ConcentrationException, SugarException{
        if(c.getTemp()>60)
              throw new TemperatureException(c.getTemp(),"Cofee is to hot!");
        if(c.getConc()>50)
              throw new ConcentrationException(c.getConc(),"Cofee concentration to high!"); 
        if(c.getSugar()>70)
            throw new SugarException("Too many coffees!",c.getSugar());       
        System.out.println("Drink cofee:"+c);
  }
}//.class


package lab8;

public class CoffeeMaker {

    Coffee makeCoffee(){
        System.out.println("Make a coffe");
        int t = (int)(Math.random()*100);
        int c = (int)(Math.random()*100);
        int su= (int)(Math.random()*100);
        Coffee coffee = new Coffee(t,c,su);
        return coffee;
  }

}//.class


package lab8;

public class CoffeeTest {

    public static void main(String[] args) {
        CoffeeMaker mk = new CoffeeMaker();
        CoffeeDrinker d = new CoffeeDrinker();

        for(int i = 0;i<15;i++){
              Coffee c = mk.makeCoffee();
              try {
                    d.drinkCoffee(c);
              } catch (TemperatureException e) {
                    System.out.println("Exception:"+e.getMessage()+" temp="+e.getTemp());
              } catch (ConcentrationException e) {
                    System.out.println("Exception:"+e.getMessage()+" conc="+e.getConc());
              } catch (SugarException e) {
                  System.out.println("Exception:"+e.getMessage()+" number="+e.getSugar());
            }
              finally{
                    System.out.println("Throw the cofee cup.\n");
              }
        }    
  }
}//.class


package lab8;

public class ConcentrationException extends Exception {

    
	int c;
    public ConcentrationException(int c,String msg) {
          super(msg);
          this.c = c;
    }

    int getConc(){
          return c;
    }
}//.class


package lab8;

public class ConcentrationException extends Exception {

    
	int c;
    public ConcentrationException(int c,String msg) {
          super(msg);
          this.c = c;
    }

    int getConc(){
          return c;
    }
}//.class


package lab8;

public class SugarException extends Exception{
	
	int su;
	public SugarException(String msg,int su)
	{
		super(msg);
		this.su=su;
	}
	 int getSugar() {
		return su;
	}

}

package lab8;

public class TemperatureException extends Exception {

	      int t;
	      public TemperatureException(int t,String msg) {
	            super(msg);
	            this.t = t;
	      }
	 
	      int getTemp(){
	            return t;
	      }
	}//.class
		