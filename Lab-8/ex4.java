package lab8;


	import java.io.Serializable;

	public class Car implements Serializable {

	    private String model;
	    private double price;

	    public Car(String model, double price) {
	        this.model = model;
	        this.price = price;
	    }

	    public String getModel() {
	        return model;
	    }

	    public double getPrice() {
	        return price;
	    }
	}


package lab8;


	import java.io.*;
	import java.util.ArrayList;

	public class Main {

	    public static int k =0;
	    public static ArrayList<Car> cars;

	    public static void saveCar(Car c){
	        try {
	            FileOutputStream fileOut = new FileOutputStream("cars.ser.txt",true);
	            ObjectOutputStream out = new ObjectOutputStream(fileOut);
	            out.writeObject(c);
	            out.close();
	            fileOut.close();
	            k++;
	            System.out.println("Serialized data is saved in cars.ser");
	        } catch (IOException i) {
	            i.printStackTrace();
	        }
	    }

	    public static void viewCars() {
	        Car c = null;
	        try {
	            FileInputStream fileIn = new FileInputStream("cars.ser.txt");
	            ObjectInputStream in = new ObjectInputStream(fileIn);
	            Car c1 = (Car)in.readObject();
	            System.out.println(c1.getModel()+" " +c1.getPrice());
	            Car c2 = (Car)in.readObject();
	            System.out.println(c2.getModel()+" " +c2.getPrice());
	            Car c3 = (Car)in.readObject();
	            System.out.println(c3.getModel()+" " +c3.getPrice());
	            in.close();
	            fileIn.close();
	        } catch (IOException i) {
	            i.printStackTrace();
	            return;
	        } catch (ClassNotFoundException ce) {
	            System.out.println("Car class not found");
	            ce.printStackTrace();
	            return;

	        }
	    }

	    public static void main(String[] args) {

	        Car c1 = new Car("Volkswagen", 6434);
	        Car c2 = new Car("Bentley", 7535);
	        Car c3 = new Car("Mercedes", 1232);

	        saveCar(c1);
	        saveCar(c2);
	        saveCar(c3);

	        viewCars();
	    }
	}

